# ebaoquan/rop_client
#### 说明
> rop_client是rop的简单实现，rop可参考：https://www.oschina.net/p/rop
#### 同java的sdk差异
```
1.java中有注解，php中没有注解和装饰器，所以一些常用的注释在php中改为了默认方法。
如：
@Ignore <->RichServiceRequest.getIgnoreSign
@Complex(style = Style.JSON) 改为了RichServiceRequest.validate()方法中验证后直接转换并赋值
```

